FROM python:2

LABEL com.nvidia.volumes.needed="nvidia_driver"

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:${PATH}

RUN pip install gbdxtools 

ADD task.py /

LABEL dustify.runargs="-e DUSTIFIED=Yes"

CMD /task.py